## Sobre 

O serviço RestCountriesProxy-api é um proxy de consulta da API [REST COUNTRIES](https://restcountries.eu/). O objetivo é fazer um cache local e reduzir a quantidade de chamadas a api externa.

O período configurado para manter o cache local é de 24hrs. Após esse periodo o cache é invalidado e uma nova consulta é realizada.

O RestCountriesProxy-api é um serviço .Net core, para mais informações execute o serviço usando `dotnet run` e acesse o [swagger](https://localhost:5001/swagger/).  



