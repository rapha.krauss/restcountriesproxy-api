using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;

namespace RestCountriesProxy.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ConsultarRotaComercialEntrePaisesController : Controller
    {
        private readonly IRotaEntrePaises _paisesService;

        public ConsultarRotaComercialEntrePaisesController(IRotaEntrePaises paisesService)
        {
            _paisesService = paisesService;
        }
        
        /// <summary>
        /// Retorna a rota comercial entre dois países do mesmo continente.
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST /api/v1/ConsultarRotaComercialEntrePaises
        ///    {
        ///        "SiglaOrigem": "RUS",
        ///        "SiglaDestino": "PRT",
        ///        "Continente": "Europe"
        ///    }
        ///
        /// </remarks>
        /// <param name="SiglaOrigem">A sigla do pais de origem</param>
        /// <param name="SiglaDestino">A sigla do pais de destino</param>
        /// <param name="Continente">O nome do continente cujo os países estão</param>
        /// <returns>Retorna a rota entre os países solicitados. Exemplo: Rússia -> Bielorrússia -> Letônia -> Lituânia -> Polônia -> República Tcheca -> áustria -> Alemanha -> Bélgica -> França -> Andorra -> Espanha -> Portugal</returns>
        /// <response code="200">Sucesso ao encontrar a rota</response>
        [HttpPost]
        public async Task<string> Post([FromBody] FilterRotaComercial filtro)
        {
            return await _paisesService.CalcularRotaComercialEntrePaises(filtro);
        }        
    }
}