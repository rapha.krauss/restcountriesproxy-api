﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RESTCountries.Models;
using RESTCountries.Services;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;
using RestCountriesProxy.Mapper;
using RestSharp.Extensions;

namespace RestCountriesProxy.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PaisesController : ControllerBase
    {
        private readonly IPaisesService _paisesService;

        public PaisesController(IPaisesService paisesService)
        {
            _paisesService = paisesService;
        }
        
        /// <summary>
        /// Consulta todos os países.
        /// </summary>
        /// <returns>Retorna uma lista de todos países encontrados</returns>
        /// <response code="200">Sucesso ao encontrar um país</response>        
        [HttpGet]
        public async Task<IEnumerable<PaisCompleto>> Get()
        {
            return await _paisesService.ObterTodosPaises();
        }

        /// <summary>
        /// Consulta países com base nos filtros .
        /// </summary>
        /// <param name="Nome">Nome do país em português</param>
        /// <param name="Sigla">A sigla do pais a ser encontrado</param>
        /// <param name="Moeda">Sigla da moeda utilizada. Por exemplo: Dólar = USD</param>
        /// <returns>Retorna uma lista de todos países encontrados</returns>
        /// <response code="200">Sucesso ao encontrar um país</response>
        [HttpPost]
        public async Task<IEnumerable<PaisCompleto>> Post([FromBody] Pais filtro)
        {
            return await _paisesService.ObterPaises(filtro);
        }
        public string Nome { get; set; }
        public string Sigla { get; set;}
        public string Moeda { get; set;}
    }
}