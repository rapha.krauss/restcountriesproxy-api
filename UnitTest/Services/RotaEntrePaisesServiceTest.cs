using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Moq;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;
using RestCountriesProxy.Services;
using UnitTest.DataFaker;
using Xunit;

namespace UnitTest.Services
{
    public class RotaEntrePaisesServiceTest
    {
        private readonly Mock<IPaisesProvider> _paisesProviderMock;
        private readonly RotaEntrePaisesService _rotaService;
        private readonly IList<PaisCompleto> _paises;

        public RotaEntrePaisesServiceTest()
        {
            _paisesProviderMock = new Mock<IPaisesProvider>();
            _paises = PaisesDataFaker.ObterLista(true);
            _paisesProviderMock.Setup(x => x.ObterTodosPaises()).ReturnsAsync(_paises);
            _rotaService = new RotaEntrePaisesService(_paisesProviderMock.Object);
        }
        
        [Theory]
        [InlineData("Americas", "USA", "BRA", "Estados Unidos -> México -> Belize -> Guatemala -> El Salvador -> Honduras -> Nicarágua -> Costa Rica -> Panamá -> Colômbia -> Brasil")]
        [InlineData("Americas", "MEX", "USA", "México -> Estados Unidos")]
        [InlineData("Europe", "RUS", "PRT", "Rússia -> Bielorrússia -> Letônia -> Lituânia -> Polônia -> República Tcheca -> áustria -> Alemanha -> Bélgica -> França -> Andorra -> Espanha -> Portugal")]
        [InlineData("americas", "mex", "usa", "México -> Estados Unidos")]//ignorecase    
        [InlineData("Oceania", "AUS", "NZL", RotaEntrePaisesService.RotaNaoEncontrada)]//Sem fronteiras
        [InlineData("Oceania", "X", "NZL", RotaEntrePaisesService.RotaNaoEncontrada)]//Origem não existe
        [InlineData("Oceania", "AUS", "X", RotaEntrePaisesService.RotaNaoEncontrada)]//Destino não existe
        [InlineData("EuroX", "AUS", "X", RotaEntrePaisesService.RotaNaoEncontrada)]//Continente não existe
        [InlineData("Africa", "RUS", "ZMB", RotaEntrePaisesService.RotaNaoEncontrada)]//Em continentes diferentes
        public async void Deve_Retornar_Rota_Entre_Paises(string continente, string siglaOrigem, string siglaDestino,
            string rotaEsperada)
        {
            var resultado = await _rotaService.CalcularRotaComercialEntrePaises(
                new FilterRotaComercial
                {
                    Continente = continente,
                    siglaDestino = siglaDestino,
                    siglaOrigem = siglaOrigem
                });

            Assert.Equal(resultado, rotaEsperada);
        }        
        
    }
}