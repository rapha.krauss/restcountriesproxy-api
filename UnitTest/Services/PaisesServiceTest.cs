using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Moq;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;
using RestCountriesProxy.Services;
using UnitTest.DataFaker;
using Xunit;

namespace UnitTest.Services
{
    public class PaisesServiceTest
    {
        private readonly Mock<IPaisesProvider> _paisesProviderMock;
        private readonly PaisesService _paisesService;
        private readonly IList<PaisCompleto> _paises;

        public PaisesServiceTest()
        {
            _paisesProviderMock = new Mock<IPaisesProvider>();
            _paises = PaisesDataFaker.ObterLista();
            _paisesProviderMock.Setup(x => x.ObterTodosPaises()).ReturnsAsync(_paises);
            _paisesService = new PaisesService(_paisesProviderMock.Object);
        }

        [Fact]
        public async void Deve_Retornar_Lista_Completa_Paises()
        {
            var atual = await _paisesService.ObterTodosPaises();
            atual.Should().BeEquivalentTo(_paises);
            _paisesProviderMock.Verify(x => x.ObterTodosPaises(), Times.Once);
        }

        [Theory]
        [InlineData("Estados Unidos", "", "", 1)]
        [InlineData("ESTADOS UNIDOS", "", "", 1)]
        [InlineData("estados unidos", "", "", 1)]
        [InlineData("estados unidos", "USA", "", 1)]
        [InlineData("estados unidos", "uSa", "", 1)]
        [InlineData("estados unidos", "USA", "USD", 1)]
        [InlineData("estados unidos", "USA", "uSd", 1)]
        [InlineData("united states", "USA", "USD", 0)]
        [InlineData("", "EUA", "USD", 0)]
        [InlineData("", "", "DLR", 0)]
        [InlineData("estados", "", "real", 0)]
        [InlineData("estados", "", "", 2)]
        [InlineData("estados", "", "USD", 2)]
        public async void Deve_Retornar_Lista_Conforme_Filtro(string nome, string sigla, string moeda,
            int qtdRegistrosEsperados)
        {
            var resultado = await _paisesService.ObterPaises(
                new Pais
                {
                    Nome = nome,
                    Sigla = sigla,
                    Moeda = moeda
                });

            Assert.Equal(resultado.Count(), qtdRegistrosEsperados);
        }
    }
}