using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Algorithms.Search;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;

namespace RestCountriesProxy.Services
{
    public class RotaEntrePaisesService: IRotaEntrePaises
    {
        private readonly IPaisesProvider _paisesProvider;
        public const string  RotaNaoEncontrada = "Não foram encontradas rotas no mesmo continente para o destino solicitado.";

        public RotaEntrePaisesService(IPaisesProvider paisesProvider)
        {
            _paisesProvider = paisesProvider;
        }
        
        public async Task<string> CalcularRotaComercialEntrePaises(FilterRotaComercial filtro)
        {
            var paises = (await _paisesProvider.ObterTodosPaises());
            var paisesContinente = paises.Where(x => x.Continente.ToUpper() == filtro.Continente.ToUpper()).ToList();
            if (OrigemDestinoInvalidos(filtro, paisesContinente))
                return RotaNaoEncontrada;
            
            var graph = InicializarPaises(filtro.siglaOrigem.ToUpper(), paises);
            InicializarFronteiras(paisesContinente, graph);
            return ObterRota(filtro.siglaOrigem.ToUpper(), filtro.siglaDestino.ToUpper(), paisesContinente, graph);
        }

        private bool OrigemDestinoInvalidos(FilterRotaComercial filtro, IList<PaisCompleto> paises)
        {
            var paisOrigem = paises.FirstOrDefault(x => x.Sigla == filtro.siglaOrigem.ToUpper());
            var paisDestino = paises.FirstOrDefault(x => x.Sigla == filtro.siglaDestino.ToUpper());

            return (paisOrigem == null || !paisOrigem.PaisesFronteira.Any() || paisDestino == null);
        }

        private string ObterRota(string siglaOrigem, string siglaDestino, IList<PaisCompleto> paises, BidirectionalGraph<string, IEdge<string>> graph)
        {
            var dfs = new DepthFirstSearchAlgorithm<string, IEdge<string>>(graph);
            var observer = new VertexPredecessorRecorderObserver<string, IEdge<string>>();
            using (observer.Attach(dfs))
                dfs.Compute();

            var rota = CalcularRotasEncontradas(siglaOrigem, siglaDestino, paises, observer);
            return String.IsNullOrEmpty(rota) ? RotaNaoEncontrada : rota;
        }

        private string CalcularRotasEncontradas(string siglaOrigem, string siglaDestino, IList<PaisCompleto> paises,
            VertexPredecessorRecorderObserver<string, IEdge<string>> observer)
        {
            IEnumerable<IEdge<string>> edges;
            var rotas = new List<string>();
            if (observer.TryGetPath(siglaDestino, out edges))
            {
                rotas.Add(ObterNome(siglaOrigem, paises));
                foreach (IEdge<string> edge in edges)
                    rotas.Add(" -> " + ObterNome(edge.Target, paises));
            }

            var rota = string.Concat(rotas.ToArray());
            return rota;
        }

        private string ObterNome(string sigla, IList<PaisCompleto> paises)
        {
            return paises.FirstOrDefault(x => x.Sigla == sigla)?.Nome;
        }

        private void InicializarFronteiras(IList<PaisCompleto> paises, BidirectionalGraph<string, IEdge<string>> graph)
        {
            foreach (var pais in paises)
                AdicionarFronteiras(pais.Sigla, pais.PaisesFronteira, graph);
        }

        private BidirectionalGraph<string, IEdge<string>> InicializarPaises(string siglaOrigem, IList<PaisCompleto> paises)
        {
            var graph = new BidirectionalGraph<string, IEdge<string>>(true);
            graph.AddVertex(siglaOrigem);
            var outrosPaises = paises.Where(x => x.Sigla != siglaOrigem);
            foreach (var pais in outrosPaises)
                graph.AddVertex(pais.Sigla);
            return graph;
        }

        private void AdicionarFronteiras(string siglaOrigem, IList<string> paisesFronteira, BidirectionalGraph<string, IEdge<string>> graph)
        {
            foreach (var paisFronteira in paisesFronteira)
            {
                graph.AddEdge(new Edge<string>(siglaOrigem, paisFronteira));
            }
        }
    }
}