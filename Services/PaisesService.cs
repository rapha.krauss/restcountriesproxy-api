using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Algorithms.Search;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;
using RestSharp.Extensions;

namespace RestCountriesProxy.Services
{
    public class PaisesService: IPaisesService
    {
        private readonly IPaisesProvider _provider;

        public PaisesService(IPaisesProvider provider)
        {
            _provider = provider;
        }
        
        public async Task<IEnumerable<PaisCompleto>> ObterTodosPaises()
        {
            return await _provider.ObterTodosPaises();
        }
        
        public async Task<IEnumerable<PaisCompleto>> ObterPaises(Pais filtro)
        {
            return FiltrarPaises(filtro, await _provider.ObterTodosPaises());
        }        

        private IList<PaisCompleto> FiltrarPaises(Pais filtro, IList<PaisCompleto> paises)
        {
            paises = FiltrarPaisPorNome(filtro, paises);
            paises = FiltrarPaisPorSigla(filtro, paises);
            paises = FiltrarPaisPorMoeda(filtro, paises);
            return paises;
        }

        private IList<PaisCompleto> FiltrarPaisPorMoeda(Pais filtro, IList<PaisCompleto> paises)
        {
            if (filtro.Moeda.HasValue())
                return paises.Where(pais => pais.Moedas.Any(moeda =>
                    moeda.Contains(filtro.Moeda, StringComparison.CurrentCultureIgnoreCase))).ToList();
            return paises;
        }

        private IList<PaisCompleto> FiltrarPaisPorSigla(Pais filtro, IList<PaisCompleto> paises)
        {
            if (filtro.Sigla.HasValue())
                return paises.Where(pais =>
                    pais.Sigla.Contains(filtro.Sigla, StringComparison.CurrentCultureIgnoreCase)).ToList();
            return paises;
        }

        private IList<PaisCompleto> FiltrarPaisPorNome(Pais filtro, IList<PaisCompleto> paises)
        {
            if (filtro.Nome.HasValue())
                return paises.Where(pais =>
                    pais.Nome.Contains(filtro.Nome, StringComparison.CurrentCultureIgnoreCase)).ToList();
            return paises;
        }
    }
}