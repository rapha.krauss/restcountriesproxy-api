using System.Threading.Tasks;
using RestCountriesProxy.Dtos;

namespace RestCountriesProxy.Abstractions
{
    public interface IRotaEntrePaises
    {
        Task<string> CalcularRotaComercialEntrePaises(FilterRotaComercial filtro);
    }
}