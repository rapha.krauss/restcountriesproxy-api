using System.Collections.Generic;
using System.Threading.Tasks;
using RestCountriesProxy.Dtos;

namespace RestCountriesProxy.Abstractions
{
    public interface IPaisesProvider
    {
        Task<IList<PaisCompleto>> ObterTodosPaises();
    }
}