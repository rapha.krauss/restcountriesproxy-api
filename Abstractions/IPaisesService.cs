using System.Collections.Generic;
using System.Threading.Tasks;
using RestCountriesProxy.Dtos;

namespace RestCountriesProxy.Abstractions
{
    public interface IPaisesService
    {
        Task<IEnumerable<PaisCompleto>> ObterTodosPaises();
        Task<IEnumerable<PaisCompleto>> ObterPaises(Pais filtro);
        
    }
}