using System.Linq;
using AutoMapper;
using RESTCountries.Models;
using RestCountriesProxy.Dtos;

namespace RestCountriesProxy.Mapper
{
    public static class PaisMap
    {
        public static IMapper Build()
        {
            return  new MapperConfiguration(cfg =>
                cfg.CreateMap<Country, PaisCompleto>()
                .ForMember(d => d.Nome, e => e.MapFrom(o => o.Translations.br))
                .ForMember(d => d.Bandeira, e => e.MapFrom(o => o.Flag))
                .ForMember(d => d.Capital, e => e.MapFrom(o => o.Capital))
                .ForMember(d => d.Linguas, e => e.MapFrom(o => o.Languages.Select( y => y.Name)))
                .ForMember(d => d.Moedas, e => e.MapFrom(o => o.Currencies.Select(y => y.Code)))
                .ForMember(d => d.Populacao, e => e.MapFrom(o => o.Population))
                .ForMember(d => d.Timezones, e => e.MapFrom(o => o.Timezones))
                .ForMember(d => d.BlocosEconomicos, e => e.MapFrom(o => o.RegionalBlocs.Select(y => y.Name)))
                .ForMember(d => d.PaisesFronteira, e => e.MapFrom(o => o.Borders))
                .ForMember(d => d.Sigla, e => e.MapFrom(o => o.Alpha3Code))
                .ForMember(d => d.Continente, e => e.MapFrom(o => o.Region))).CreateMapper();
        }
    }
}