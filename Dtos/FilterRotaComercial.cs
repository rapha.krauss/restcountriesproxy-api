namespace RestCountriesProxy.Dtos
{
    public class FilterRotaComercial
    {
        public string siglaOrigem { get; set; }
        public string siglaDestino { get; set; }
        
        public string Continente { get; set; }
    }
}