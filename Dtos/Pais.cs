namespace RestCountriesProxy.Dtos
{
    public class Pais
    {
        public string Nome { get; set; }
        public string Sigla { get; set;}
        public string Moeda { get; set;}
    }
}