using System.Collections.Generic;
using Remotion.Linq.Clauses.StreamedData;

namespace RestCountriesProxy.Dtos
{
    /*
 * Crie uma consulta de pesquisa com base no nome do país, sigla ou moeda. A consulta deve
 * retornar uma lista de países com os seguintes campos: Nome do País, Sigla, Moedas (sigla),
    Bandeira e Blocos Econômicos);
    d) Permita que ao clicar em um país na interface anterior, sejam exibidas as seguintes
    informações extras do país: População, Timezone, Moedas (currencies), Línguas(languages),
    Capital, Blocos econômicos (Regional Blocks) e Países que fazem fronteira (bordering
    countries);
 */
    public class PaisCompleto: Pais
    {
        public IList<string> Moedas { get; set; }
        public string Bandeira { get; set; }
        public IList<string> BlocosEconomicos { get; set; }
        public int Populacao { get; set; }
        public IList<string> Timezones { get; set; }
        public IList<string> Linguas { get; set; }
        public string Capital { get; set; }
        public IList<string> PaisesFronteira { get; set; }
        
        public string Continente { get; set; }
    }
}