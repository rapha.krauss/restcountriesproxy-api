﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace restcountriesprox
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddMemoryCache();
            services.TryAddSingleton<IPaisesProvider, PaisesProvider>();
            services.TryAddScoped<IPaisesService, PaisesService>();
            services.TryAddScoped<IRotaEntrePaises, RotaEntrePaisesService>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo{ Title = "Rest Countries Proxy", Version = "v1",
                    Description = "O serviço RestCountriesProxy-api é um proxy de consulta da API REST COUNTRIES.", });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Rest Countries Proxy V1"); });
            
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();
            app.UseMvc();

        }
    }
}