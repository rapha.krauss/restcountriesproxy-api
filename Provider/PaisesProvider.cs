using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using RESTCountries.Models;
using RESTCountries.Services;
using RestCountriesProxy.Abstractions;
using RestCountriesProxy.Dtos;
using RestCountriesProxy.Mapper;

namespace RestCountriesProxy.Services
{
    public class PaisesProvider: IPaisesProvider
    {
        private readonly IMemoryCache _cache;
        private const string ChavePadrao = "ListCountries";
        private const int TempoInvalidacaoCacheHoras = 24;

        public PaisesProvider(IMemoryCache cache)
        {
            _cache = cache;
        }
        public async Task<IList<PaisCompleto>> ObterTodosPaises()
        {
            var countries = await ObterPaisesCache();
            return PaisMap.Build().Map<IEnumerable<Country>, IEnumerable<PaisCompleto>>(countries).ToList();
        }

        private async Task<List<Country>> ObterPaisesCache()
        {
            List<Country> countries;
            if (!_cache.TryGetValue(ChavePadrao, out countries))
            {
                countries = await RESTCountriesAPI.GetAllCountriesAsync();
                _cache.Set(ChavePadrao, countries, CacheOptions());
            }
            return countries;
        }

        private static MemoryCacheEntryOptions CacheOptions()
        {
            var options = new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromHours(TempoInvalidacaoCacheHoras));
            return options;
        }
    }
}